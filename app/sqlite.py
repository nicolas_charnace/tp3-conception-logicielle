import sqlite3


# On peut choisir différents types de date pour sqlLite https://www.sqlitetutorial.net/sqlite-date/

def init():
  connection = sqlite3.connect('db')
  cursor = connection.cursor()
  requetecreationTableRequetes = """CREATE TABLE IF NOT EXISTS requetes(requete_id INTEGER PRIMARY KEY AUTOINCREMENT, date_text text)"""
  cursor.execute(requetecreationTableRequetes)
  
def insertRequestIntoDB():
  connection = sqlite3.connect('db')
  cursor = connection.cursor()
  requeteLogTable="""INSERT INTO requetes(date_text) VALUES(datetime('now'))"""
  cursor.execute(requeteLogTable)
